package ants.mobile.insight;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

import ants.mobile.ants_insight.Constants.Events;
import ants.mobile.ants_insight.Insights;
import ants.mobile.ants_insight.Model.UserItem;
import ants.mobile.insights.R;

public class MainActivity extends Activity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        finish();
        UserItem userItem = new UserItem();
        userItem.setEmail("luonglc4@gmail.com", false);
        userItem.setUserName("luonglc4", false);


        Insights insights = new Insights.Builder().setInstance(this).build();
        insights.logEvent(Events.IDENTIFY, userItem);
//        ItemCustom itemCustom = new ItemCustom();
//        itemCustom.setItemType("member_card");
//        itemCustom.setItemName("Card Name");
//        itemCustom.setItemId("CARD_NUMBER");
//
//        List<ItemCustom> itemCustomList = new ArrayList<>();
//        itemCustomList.add(itemCustom);
//
//        Extra extra = new Extra();
//        extra.setFirst_name("USER_FIRST_NAME");
//        extra.setLast_name("USER_LAST_NAME");
//        extra.setEmail("USER_EMAIL");
//        extra.setPhone("Phone_Number");
//        extra.setCustomer_id("CARD_NUMBER_ID");
//        extra.setBirthday("USER_BIRTH_DAY");
        //type("link_card"/"no_card") don't need to set because it automatic set when it check itemCustomList null or not
//        insights.logEventCustom("category","action",null,extra);
//        insights.logEventCustom("Screen 1");
//        insights.logEvent(Events.SCREEN_VIEW);
//        Extra extraItem = new Extra.Builder()
//                .phone("Phone_Number")
//                .customer_id("CARD_NUMBER_ID")
//                .build();
//        insights.logEventCustom(Events.SIGN_IN,extraItem);
//        try {
//        JSONObject jo = new JSONObject();
//            jo.put("item_id", "CARD_NUMBER");
//            jo.put("item_name", "Card Name");
//            jo.put("item_type", "member_card");
//            JSONArray jsonUserItem = new JSONArray();
//            jsonUserItem.put(jo);
//
//            JSONObject extraObject = new JSONObject();
//            extraObject.put("earning_point_type","Invite_friend");
//            extraObject.put("point_added","POINT_ADDE_NUMBER");
//            extraObject.put("customer_id","CARD_NUMBER_ID");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//================================================================================
//        Insights insights1 = new Insights.Builder().setInstance(this).build();
//        ItemCustom itemCustom1 = new ItemCustom.Builder()
//                .itemId("CARD_NUMBER")
//                .itemName("Card Name")
//                .itemType("member_card").build();
//        List<ItemCustom> itemCustomList1 = new ArrayList<>();
//        itemCustomList1.add(itemCustom1);
//
//        Extra extraItem = new Extra.Builder()
//                .phone("Phone_Number")
//                .customer_id("CARD_NUMBER_ID")
//                .first_name("USER_FIRST_NAME")
//                .last_name("USER_LAST_NAME")
//                .email("USER_EMAIL")
//                .birthday("USER_BIRTH_DAY")
//                .build();
//        insights1.logEventCustom(Events.SIGN_UP,null,extraItem);
//        //==================================================================================
//        Extra extra1 = new Extra();
//        extra.setCustomer_id("CARD_NUMBER_ID");
//        insights.logEventCustom(Events.SIGN_OUT,extra1);
//
//        Extra extra2 = new Extra.Builder().customer_id("CARD_NUMBER_ID").build();
//        insights.logEventCustom(Events.SIGN_OUT,extra2);
//
//        //=============================================================================
//        ItemCustom itemCustom2 = new ItemCustom();
//        itemCustom2.setItemType("member_card");
//        itemCustom2.setItemName("Card Name");
//        itemCustom2.setItemId("CARD_NUMBER");
//
//        List<ItemCustom> itemCustomList2 = new ArrayList<>();
//        itemCustomList2.add(itemCustom2);
//
//        Extra extra3 = new Extra();
//        extra3.setEarning_point_type("Invite_friend");
//        extra3.setPoint_added("POINT_ADDE_NUMBER");
//        extra3.setCustomer_id("CARD_NUMBER_ID");
//
//        insights.logEventCustom(Events.POINT_EARNING,itemCustomList2,extra3);
//
//        ItemCustom itemCustom3 = new ItemCustom.Builder()
//                .itemId("CARD_NUMBER")
//                .itemName("Card Name")
//                .itemType("member_card")
//                .build();
//        List<ItemCustom> itemCustomList3 = new ArrayList<>();
//        itemCustomList3.add(itemCustom3);
//
//        Extra extraItem1 = new Extra.Builder()
//                .earning_point_type("Invite_friend")
//                .point_added("POINT_ADDE_NUMBER")
//                .customer_id("CARD_NUMBER_ID")
//                .build();
//        insights.logEventCustom(Events.POINT_EARNING,itemCustomList3,extraItem1);
//        //=============================================================
//
//        ItemCustom itemCustom4 = new ItemCustom();
//        itemCustom4.setItemId("");
//        itemCustom4.setItemName("");
//        itemCustom4.setItemType("");
//        itemCustom4.setCoupon_value("");
//        itemCustom4.setCoupon_date_expired("");
//        itemCustom4.setCoupon_type("");
//        List<ItemCustom> itemCustomList4 = new ArrayList<>();
//        itemCustomList4.add(itemCustom4);
//
//        Extra extraItem3 = new Extra();
//        extraItem3.setCustomer_id("CARD_NUMBER_ID");
//        insights.logEventCustom(Events.COUPON_LIST_VIEW,itemCustomList4,extraItem3);
//
//        ItemCustom itemCustom5 = new ItemCustom.Builder()
//                .itemId("X")
//                .itemName("Discount 50%")
//                .itemType("coupon")
//                .coupon_value("X_NUMBER")
//                .coupon_date_expired("yyyy-MM-dd HH:mm:ss")
//                .coupon_type("COUPON_TYPE").build();
//        List<ItemCustom> itemCustomList5 = new ArrayList<>();
//        itemCustomList5.add(itemCustom5);
//        Extra extraItem4 = new Extra.Builder().customer_id("CARD_NUMBER_ID").build();
//        insights.logEventCustom(Events.COUPON_LIST_VIEW,itemCustomList5,extraItem4);
//
//        //========================================================================
//        ItemCustom itemCustom6 = new ItemCustom();
//        itemCustom6.setItemId("item_name");
//        itemCustom6.setItemName("Discount 50%");
//        itemCustom6.setItemType("coupon_value");
//        itemCustom6.setCoupon_value("X_NUMBER");
//        itemCustom6.setCoupon_date_expired("yyyy-MM-dd HH:mm:ss");
//        itemCustom6.setCoupon_type("COUPON_TYPE");
//        List<ItemCustom> itemCustomList6 = new ArrayList<>();
//        itemCustomList6.add(itemCustom6);
//        insights.logEventCustom(Events.COUPON_ADD_FAVORITE,itemCustomList6);
//
//        ItemCustom itemCustom7 = new ItemCustom.Builder()
//                .itemId("item_name")
//                .itemName("Discount 50%")
//                .itemType("coupon_value")
//                .coupon_value("X_NUMBER")
//                .coupon_date_expired("yyyy-MM-dd HH:mm:ss")
//                .coupon_type("COUPON_TYPE").build();
//        List<ItemCustom> itemCustomList7 = new ArrayList<>();
//        itemCustomList7.add(itemCustom7);
//        insights.logEventCustom(Events.COUPON_REDEEM_COMPLETE,itemCustomList7);
    }
}