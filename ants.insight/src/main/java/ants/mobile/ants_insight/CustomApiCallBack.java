package ants.mobile.ants_insight;

import android.annotation.SuppressLint;
import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * create a Observer to listening reponse from delivery
 * @param <R>
 */
public class CustomApiCallBack<R> implements Observer<R> {

    @Override
    public void onSubscribe(Disposable d) {

    }

    @SuppressLint("LongLogTag")
    @Override
    public void onNext(R r) {
            Log.d("<<<<<<INSIGHT_SDK", r.toString());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onError(Throwable e) {
            Log.e("<<<<<<<<INSIGHT_SDK", e.getMessage());
    }

    @Override
    public void onComplete() {
    }
}
