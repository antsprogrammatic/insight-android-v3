package ants.mobile.ants_insight.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class ItemCustom {
    String itemId;
    String itemName;
    String itemType;
    String coupon_value;
    String coupon_date_expired;
    String coupon_type;
    String point;
    String date_activate;

    public ItemCustom(String itemId, String itemName, String itemType, String coupon_value, String coupon_date_expired, String coupon_type, String point, String date_activate) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemType = itemType;
        this.coupon_value = coupon_value;
        this.coupon_date_expired = coupon_date_expired;
        this.coupon_type = coupon_type;
        this.point = point;
        this.date_activate = date_activate;
    }

    public ItemCustom() {}

    JSONObject getItemInfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("item_type", itemType);
            jsonObject.put("item_name", itemName);
            jsonObject.put("item_id", itemId);
            jsonObject.put("coupon_value", coupon_value);
            jsonObject.put("coupon_date_expired", coupon_date_expired);
            jsonObject.put("coupon_type", coupon_type);
            jsonObject.put("point", point);
            jsonObject.put("date_activate", date_activate);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static class Builder{
        String itemId;
        String itemName;
        String itemType;
        String coupon_value;
        String coupon_date_expired;
        String coupon_type;
        String point;
        String date_activate;

        public Builder() {}
        public Builder itemId(String itemId) {
            this.itemId = itemId;
            return this;
        }
        public Builder itemName(String itemName) {
            this.itemName = itemName;
            return this;
        }
        public Builder itemType(String itemType) {
            this.itemType = itemType;
            return this;
        }
        public Builder coupon_value(String coupon_value) {
            this.coupon_value = coupon_value;
            return this;
        }
        public Builder coupon_date_expired(String coupon_date_expired) {
            this.coupon_date_expired = coupon_date_expired;
            return this;
        }
        public Builder coupon_type(String coupon_type) {
            this.coupon_type = coupon_type;
            return this;
        }
        public Builder point(String point) {
            this.point = point;
            return this;
        }
        public Builder date_activate(String date_activate) {
            this.date_activate = date_activate;
            return this;
        }
        public ItemCustom build() {
            return new ItemCustom(this);
        }
    }
    public ItemCustom(Builder builder){
        this.itemId = builder.itemId;
        this.itemName = builder.itemName;
        this.itemType = builder.itemType;
        this.coupon_value = builder.coupon_value;
        this.coupon_date_expired = builder.coupon_date_expired;
        this.coupon_type = builder.coupon_type;
        this.point = builder.point;
        this.date_activate = builder.date_activate;
    }
    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getCoupon_value() {
        return coupon_value;
    }

    public void setCoupon_value(String coupon_value) {
        this.coupon_value = coupon_value;
    }

    public String getCoupon_date_expired() {
        return coupon_date_expired;
    }

    public void setCoupon_date_expired(String coupon_date_expired) {
        this.coupon_date_expired = coupon_date_expired;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(String coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getDate_activate() {
        return date_activate;
    }

    public void setDate_activate(String date_activate) {
        this.date_activate = date_activate;
    }
}
