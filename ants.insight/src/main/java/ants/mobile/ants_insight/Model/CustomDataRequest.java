package ants.mobile.ants_insight.Model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import ants.mobile.ants_insight.Anonymous;
import ants.mobile.ants_insight.Constants.Constants;
import ants.mobile.ants_insight.Constants.Events;
import ants.mobile.ants_insight.InsightSDK;
import ants.mobile.ants_insight.InsightSharedPref;

import static ants.mobile.ants_insight.Constants.Constants.PREF_ANDROID_APP_PUSH_ID;
import static ants.mobile.ants_insight.Constants.Constants.PREF_IS_FIRST_INSTALL_APP;

public class CustomDataRequest {
    private List<ItemCustom> itemCustomList;
    private ContextModel contextModel;
    private Extra customExtraItem;
    private List<Dimension> dimensionList;
    private static final Long INTERVAL_REFRESH_SECTION = 1800000L; // milli second
    private Context mContext;
    public Events eventActionCustom;
    public String eventCategoryCustom;
    public String eventActionNameCustom;
    public String stringEventAction;
    public String stringCategory;

    public JSONArray jsonItemCustom;
    public JSONObject jsonItemObject;
    private static String sections = "";

    public static class Builder {
        private List<ItemCustom> itemCustomList;
        private Extra customExtraItem;
        private JSONArray jsonItemCustom;
        private JSONObject jsonItemObject;
        private List<Dimension> dimensionList;
        private Events eventActionCustom;
        private Context context;
        private String stringEventAction;
        private String stringCategory;

        public Builder eventActionCustom(Events eventActionCustom) {
            this.eventActionCustom = eventActionCustom;
            return this;
        }

        public Builder eventActionCustom(String customEvent){
            this.stringEventAction = customEvent;
            return this;
        }
        public Builder eventCategory(String customCategory){
            this.stringCategory = customCategory;
            return this;
        }

        public Builder dimensionList(List<Dimension> dimensionList) {
            this.dimensionList = dimensionList;
            return this;
        }
        public Builder itemCustomList(List<ItemCustom> itemCustomList) {
            this.itemCustomList = itemCustomList;
            return this;
        }
        public Builder itemCustomList(JSONArray itemCustomList) {
            this.jsonItemCustom = itemCustomList;
            return this;
        }
        public Builder extraItem(Extra extras) {
            this.customExtraItem = extras;
            return this;
        }
        public Builder extraItem(JSONObject extras) {
            this.jsonItemObject = extras;
            return this;
        }

        public Builder setInstance(Context context) {
            this.context = context;
            return this;
        }

        public CustomDataRequest build() {
            return new CustomDataRequest(this);
        }
    }

    private CustomDataRequest(Builder builder) {
        this.mContext = builder.context;
        this.contextModel = new ContextModel(builder.context);
        this.itemCustomList = builder.itemCustomList;
        this.dimensionList = builder.dimensionList;
        this.customExtraItem = builder.customExtraItem;
        this.eventActionCustom = builder.eventActionCustom;
        this.stringCategory = builder.stringCategory;
        this.stringEventAction = builder.stringEventAction;
        this.jsonItemCustom = builder.jsonItemCustom;
        this.jsonItemObject = builder.jsonItemObject;
        if (eventActionCustom != null)
            initEvent(eventActionCustom);

    }

    /**
     * Convert data to param-key
     *
     * @return JSonObject
     */

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public JSONObject getJSONObjectData() {
        JSONObject param = new JSONObject();
        try {
            param.put("uid", getUID());
            param.put("aid", getUUID());
            param.put("sid", getSectionId());
            param.put("ea",eventActionNameCustom);
            param.put("ec", eventCategoryCustom );
            param.putOpt("context", contextModel.getContextModel());
            if (itemCustomList != null && itemCustomList.size() > 0){
                param.putOpt("items", getItems());
                if (InsightSharedPref.getBooleanValue(PREF_IS_FIRST_INSTALL_APP)) {
                    JSONObject extraParam = new JSONObject();
                    extraParam.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
                    param.putOpt("extra", extraParam);
                    InsightSharedPref.savePreference(PREF_IS_FIRST_INSTALL_APP, false);
                } else {
                    if (customExtraItem != null){
                        if (eventActionCustom == Events.SIGN_UP)
                            customExtraItem.setType("link_card");
                        param.putOpt("extra", getExtras());
                    }
                }
            }
            else {
                if (customExtraItem != null){
                    if (eventActionCustom == Events.SIGN_UP)
                        customExtraItem.setType("no_card");
                    param.putOpt("extra", getExtras());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public JSONObject getJSONObjectCustom() {
        JSONObject param = new JSONObject();
        try {
            param.put("uid", getUID());
            param.put("aid", getUUID());
            param.put("sid", getSectionId());
            param.put("ea",stringEventAction);
            param.put("ec", stringCategory );
            param.putOpt("context", contextModel.getContextModel());
            if (jsonItemObject!=null){
                param.putOpt("items", jsonItemObject);
                if (InsightSharedPref.getBooleanValue(PREF_IS_FIRST_INSTALL_APP)) {
                    JSONObject extraParam = new JSONObject();
                    extraParam.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
                    param.putOpt("extra", extraParam);
                    InsightSharedPref.savePreference(PREF_IS_FIRST_INSTALL_APP, false);
                }
                else {
                    if (jsonItemCustom!=null)
                        param.putOpt("extra", jsonItemCustom);
                }
            }else {
                if (jsonItemCustom!=null)
                    param.putOpt("extra", jsonItemCustom);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public JSONObject getJSONObjectData(String stringCategory,String stringEventAction){
        JSONObject param = new JSONObject();
        try{
            param.put("uid", getUID());
            param.put("aid", getUUID());
            param.put("sid", getSectionId());
            param.put("ea",stringEventAction);
            param.put("ec", stringCategory );
            param.putOpt("context", contextModel.getContextModel());
            if (itemCustomList != null && itemCustomList.size() > 0){
                param.putOpt("items", getItems());
                if (InsightSharedPref.getBooleanValue(PREF_IS_FIRST_INSTALL_APP)) {
                    JSONObject extraParam = new JSONObject();
                    extraParam.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
                    param.putOpt("extra", extraParam);
                    InsightSharedPref.savePreference(PREF_IS_FIRST_INSTALL_APP, false);
                } else {
                    if (customExtraItem != null){
                        if (stringEventAction == Events.SIGN_UP.toString())
                            customExtraItem.setType("link_card");
                        param.putOpt("extra", getExtras());
                    }
                }
            }
            else {
                if (customExtraItem != null){
                    if (stringEventAction == Events.SIGN_UP.toString())
                        customExtraItem.setType("no_card");
                    param.putOpt("extra", getExtras());
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        return param;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public JSONObject getJSONObjectData(String stringEventAction){
        JSONObject param = new JSONObject();
        try{
            param.put("uid", getUID());
            param.put("aid", getUUID());
            param.put("sid", getSectionId());
            param.put("ea",stringEventAction);
            param.putOpt("context", contextModel.getContextModel());
            if (itemCustomList != null && itemCustomList.size() > 0){
                param.putOpt("items", getItems());
                if (InsightSharedPref.getBooleanValue(PREF_IS_FIRST_INSTALL_APP)) {
                    JSONObject extraParam = new JSONObject();
                    extraParam.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
                    param.putOpt("extra", extraParam);
                    InsightSharedPref.savePreference(PREF_IS_FIRST_INSTALL_APP, false);
                } else {
                    if (customExtraItem != null){
                        if (stringEventAction == Events.SIGN_UP.toString())
                            customExtraItem.setType("link_card");
                        param.putOpt("extra", getExtras());
                    }
                }
            }
            else {
                if (customExtraItem != null){
                    if (stringEventAction == Events.SIGN_UP.toString())
                        customExtraItem.setType("no_card");
                    param.putOpt("extra", getExtras());
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        return param;
    }


    private String getUID() {
        String uid;
        if (getAnonymousIndex() != 0)
            uid = getUUID() + "_" + (getAnonymousIndex());
        else
            uid = getUUID();
        return uid;
    }

    private String getNewUID() {
        String uid;
        uid = getUUID() + "_" + (getAnonymousIndex() + 1);
        return uid;
    }

    @SuppressLint("HardwareIds")
    private String getUUID() {
        UUID androidId_UUID = null;
        String androidId = Settings.Secure.getString(InsightSDK.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        try {
            androidId_UUID = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return androidId_UUID != null ? androidId_UUID.toString().toUpperCase() : "";
    }

    private void initEvent(@NonNull Events eventAction) {
        switch (eventAction) {
            case IDENTIFY:
            case SIGN_OUT:
            case SIGN_IN:
                eventCategoryCustom = Constants.USER_IDENTIFY_CATEGORY_CUSTOM;
                eventActionNameCustom = eventAction.getValue();
                InsightSharedPref.savePreference(Constants.PREF_IS_PUSH_PLAYER_ID, true);
                break;
            case SCREEN_VIEW:
                    eventCategoryCustom = Constants.SCREEN_VIEW_CATEGORY_CUSTOM;
                    eventActionNameCustom = eventAction.getValue();
                break;
            case PRODUCT_SEARCH:
                    eventCategoryCustom = Constants.STORE_CATEGORY_CUSTOM;
                    eventActionNameCustom = "search";
                break;
            case VIEW_CART:
                eventCategoryCustom = Constants.CART_CUSTOM;
                eventActionNameCustom = "view";
                break;
            case SIGN_UP:
                eventCategoryCustom = Constants.USER_IDENTIFY_CATEGORY_CUSTOM;
                eventActionNameCustom = eventAction.getValue();
                break;
            case VIEW_PRODUCT_DETAIL:
            case COUPON_LIST_VIEW:
            case COUPON_ADD_FAVORITE:
            case COUPON_REDEEM:
            case COUPON_REDEEM_COMPLETE:
                    eventCategoryCustom = Constants.COUPON_CUSTOM;
                    eventActionNameCustom = eventAction.getValue();
                break;
            case POINT_EARNING:
                    eventCategoryCustom = Constants.POINT_CUSTOM;
                    eventActionNameCustom =  eventAction.getValue();
                break;
            default:
                break;
        }
    }

    private JSONArray getItems() {
        if (itemCustomList == null)
            return null;
        JSONArray array = new JSONArray();
        for (ItemCustom productItem : itemCustomList) {
            array.put(productItem.getItemInfo());
        }
        return array;
    }
    private JSONObject getExtras() {
        return customExtraItem.getExtraInfo();
    }

       private JSONObject getDimension() {
        if (dimensionList == null)
            return null;
        JSONObject dimensionObject = new JSONObject();
        for (Dimension dimension : dimensionList) {
            try {
                dimensionObject.putOpt(dimension.getDimensionCategory(), dimension.getDimensionObject());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return dimensionObject;
    }
    private int getAnonymousIndex() {
        int index = 0;
        String indexFromFile = Anonymous.getInstance().getIndexFromStorageLocal(mContext);
        if (TextUtils.isEmpty(indexFromFile)) {
            return 0;
        } else {
            try {
                index = Integer.parseInt(indexFromFile);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return index;
    }
    private String getSectionId() {
        if (TextUtils.isEmpty(sections)) sections = getSections();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                sections = getSections();
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, INTERVAL_REFRESH_SECTION);
        return sections;
    }
    private static String getSections() {
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmm", Locale.getDefault());
        return formatter.format(todayDate);
    }
}
