package ants.mobile.ants_insight.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class Extra {
    String type;
    String phone;
    String first_name;
    String last_name;
    String email;
    String birthday;
    String customer_id;
    String earning_point_type;
    String pin_code;
    String point_sub;
    String src_search_term;
    String point_added;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getEarning_point_type() {
        return earning_point_type;
    }

    public void setEarning_point_type(String earning_point_type) {
        this.earning_point_type = earning_point_type;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getPoint_sub() {
        return point_sub;
    }

    public void setPoint_sub(String point_sub) {
        this.point_sub = point_sub;
    }

    public String getSrc_search_term() {
        return src_search_term;
    }

    public void setSrc_search_term(String src_search_term) {
        this.src_search_term = src_search_term;
    }

    public String getPoint_added() {
        return point_added;
    }

    public void setPoint_added(String point_added) {
        this.point_added = point_added;
    }

    public Extra(String type, String phone, String firstName, String lastName, String email,
                 String birthday, String customerID, String earning_point_type, String pin_code,
                 String point_sub, String src_search_term, String point_added) {
        this.type = type;
        this.phone = phone;
        this.first_name = firstName;
        this.last_name = lastName;
        this.email = email;
        this.birthday = birthday;
        this.customer_id = customerID;
        this.earning_point_type = earning_point_type;
        this.pin_code = pin_code;
        this.point_sub = point_sub;
        this.src_search_term = src_search_term;
        this.point_added = point_added;
    }

    public Extra() {
    }

    JSONObject getExtraInfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", type);
            jsonObject.put("phone", phone);
            jsonObject.put("first_name", first_name);
            jsonObject.put("last_name", last_name);
            jsonObject.put("email", email);
            jsonObject.put("customer_id", customer_id);
            jsonObject.put("birthday", birthday);
            jsonObject.put("earning_point_type", earning_point_type);
            jsonObject.put("pin_code", pin_code);
            jsonObject.put("point_sub", point_sub);
            jsonObject.put("src_search_term", src_search_term);
            jsonObject.put("point_added", point_added);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
    public static class Builder {

        String type;
        String phone;
        String first_name;
        String last_name;
        String email;
        String birthday;
        String customer_id;
        String earning_point_type;
        String pin_code;
        String point_sub;
        String src_search_term;
        String point_added;

        public Builder() { }

        public Builder type(String type) {
            this.type = type;
            return this;
        }
        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }
        public Builder first_name(String first_name) {
            this.first_name = first_name;
            return this;
        }
        public Builder last_name(String last_name) {
            this.last_name = last_name;
            return this;
        }
        public Builder email(String email) {
            this.email = email;
            return this;
        }
        public Builder birthday(String birthday) {
            this.birthday = birthday;
            return this;
        }
        public Builder customer_id(String customer_id) {
            this.customer_id = customer_id;
            return this;
        }
        public Builder earning_point_type(String earning_point_type) {
            this.earning_point_type = earning_point_type;
            return this;
        }
        public Builder pin_code(String pin_code) {
            this.pin_code = pin_code;
            return this;
        }
        public Builder point_sub(String point_sub) {
            this.point_sub = point_sub;
            return this;
        }
        public Builder src_search_term(String src_search_term) {
            this.src_search_term = src_search_term;
            return this;
        }
        public Builder point_added(String point_added) {
            this.point_added = point_added;
            return this;
        }

        public Extra build() {
            return new Extra(this);
        }

    }

    private Extra(Builder builder) {
        this.type = builder.type;
        this.phone = builder.phone;
        this.first_name = builder.first_name;
        this.last_name = builder.last_name;
        this.email = builder.email;
        this.birthday = builder.birthday;
        this.customer_id = builder.customer_id;
        this.earning_point_type = builder.earning_point_type;
        this.pin_code = builder.pin_code;
        this.point_sub = builder.point_sub;
        this.src_search_term = builder.src_search_term;
        this.point_added = builder.point_added;
    }

}
