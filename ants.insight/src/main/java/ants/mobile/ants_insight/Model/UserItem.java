package ants.mobile.ants_insight.Model;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import ants.mobile.ants_insight.Constants.Constants;
import ants.mobile.ants_insight.adx.Utils;
import ants.mobile.ants_insight.InsightSharedPref;

import static ants.mobile.ants_insight.Constants.Constants.PREF_ANDROID_APP_PUSH_ID;

public class UserItem {
    private String userName;
    private String email;
    private String address;
    private String lastName;
    private String firstName;
    private String phone;
    private String customerId;
    private String gender;
    private String birthday;
    private String playerId;
    private String avatar;
    private String userDescription;
    private List<Other> otherList;
    private String phoneId;
    private String emailId;
    private boolean idEncode;

    public UserItem(String userName, String email, String address, String lastName, String firstName, String phone, String customerId,
                    String gender, String birthday, String avatar, String userDescription) {
        this.userName = userName;
        this.email = email;
        this.address = address;
        this.lastName = lastName;
        this.firstName = firstName;
        this.phone = phone;
        this.customerId = customerId;
        this.gender = gender;
        this.birthday = birthday;
        this.avatar = avatar;
        this.userDescription = userDescription;
        this.phoneId = phone;
        this.emailId = email;
    }

    public UserItem() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    JSONObject getUserInfo() {
        JSONObject param = new JSONObject();
        try {
            param.put("type", "lead");
            param.put("id", convertStringToMD5(emailId));
            param.put("name", firstName + lastName);
            param.put("first_name", firstName);
            param.put("last_name", lastName);
            param.put("phone", phone);
            param.put("email", email);
            param.put("birthday", birthday);
            param.put("address", address);
            param.put("username", userName);
            param.put("gender", gender);
            param.put("customer_id", customerId);
            param.put("avatar", avatar);
            param.put("description", userDescription);

            if (InsightSharedPref.getBooleanValue(Constants.PREF_IS_PUSH_PLAYER_ID)) {
                param.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
                InsightSharedPref.savePreference(Constants.PREF_IS_PUSH_PLAYER_ID, false);
            }
            if (otherList != null && otherList.size() > 0) {
                for (Other other : otherList) {
                    param.put(other.key, other.value);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;
    }

    public void setEmail(String email, boolean isEncode) {
        this.email = !isEncode ? email : convertStringToMD5(email);
    }

    public void setAddress(String address, boolean isEncode) {
        this.address = !isEncode ? address : convertStringToMD5(address);
    }

    public void setLastName(String lastName, boolean isEncode) {
        this.lastName = !isEncode ? lastName : convertStringToMD5(lastName);
    }

    public void setUserName(String userName, boolean isEncode) {
        this.userName = !isEncode ? userName : convertStringToMD5(userName);
    }

    public void setFirstName(String firstName, boolean isEncode) {
        this.firstName = !isEncode ? firstName : convertStringToMD5(firstName);
    }

    public void setPhone(String phone, boolean isEncode) {
        this.phone = !isEncode ? phone : convertStringToMD5(phone);
    }

    public void setGender(String gender, boolean isEncode) {
        this.gender = !isEncode ? gender : convertStringToMD5(gender);
    }

    public void setBirthday(String birthday, boolean isEncode) {
        this.birthday = !isEncode ? birthday : convertStringToMD5(birthday);
    }

    public void setCustomerId(String customerId, boolean isEncode) {
        this.customerId = !isEncode ? customerId : convertStringToMD5(customerId);
    }

    public List<Other> getOtherList() {
        return otherList;
    }

    public void setOtherList(List<Other> otherList) {
        this.otherList = otherList;
    }

    private static String convertStringToMD5(final String input) {
        if (!TextUtils.isEmpty(input)) {
            try {
                // Create MD5 Hash
                MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
                digest.update(input.getBytes());
                byte[] messageDigest = digest.digest();

                // Create Hex String
                StringBuilder hexString = new StringBuilder();
                for (byte b : messageDigest) hexString.append(Integer.toHexString(0xFF & b));

                return hexString.toString();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public void setIdEncode(boolean idEncode) {
        this.idEncode = idEncode;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }
}
