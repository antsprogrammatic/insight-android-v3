package ants.mobile.ants_insight.Constants;

public enum Events {
    PRODUCT_SEARCH("product_search"),
    PRODUCT_LIST_VIEW("product_list_view"),
    PRODUCT_LIST_FILTER("product_list_filter"),
    PRODUCT_CLICK("click"),
    VIEW_PRODUCT_DETAIL("view"),
    ADD_TO_CART("add_to_cart"),
    REMOVE_CART("remove_cart"),
    VIEW_CART("view_cart"),
    CHECKOUT("checkout"),
    PAYMENT("payment"),
    PURCHASE("purchase"),
    SCREEN_VIEW("view"),
    IDENTIFY("identify"),
    SIGN_IN("sign_in"),
    SIGN_UP("sign_up"),
    SIGN_OUT("sign_out"),
    IMPRESSION("impression"),
    VIEWABLE("view"),
    ADX_CLICK("view"),
    VIEW_COMPARE_LIST("compare_list"),
    VIEW_WISH_LIST("view_wish_list"),
    ADD_COMPARE("add_compare"),
    ADD_WISH_LIST("add_wish_list"),
    RESET_ANONYMOUS_ID("reset_anonymous_id"),
    COUPON_LIST_VIEW("list_view"),
    COUPON_ADD_FAVORITE("add_to_favorite"),
    COUPON_REDEEM("redeem_click"),
    COUPON_REDEEM_COMPLETE("redeem_completed"),
    POINT_EARNING("earning");


    private final String value;

    Events(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
